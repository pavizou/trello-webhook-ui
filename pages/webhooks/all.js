import nookies from 'nookies';
import { EditableWebhook } from './[modelId]';
import Error from '../../components/error';
import Title from '../../components/title';

export default function Webhooks({ webhooks, error }) {
  if (typeof webhooks === 'undefined') return null;
  if (error) {
    return (
      <Error error={error} />
    );
  }
  return (
    <div>
      <Title>All your webhooks:</Title>
      <div className="flex flex-wrap container max-w-full sm:ml-4">
        {
          webhooks.map((webhook) => {
            return (
                <EditableWebhook webhook={webhook} key={webhook.id} modelId={webhook.modelId} />
            );
          })
        }
      </div>
    </div>
  )
}

export async function getServerSideProps(ctx) {
  const cookies = nookies.get(ctx);

  try {
    const res = await fetch(`https://api.trello.com/1/tokens/${cookies.trello_tkn}/webhooks?key=${cookies.trello_app_key || process.env.NEXT_PUBLIC_TRELLO_KEY}`);

    if (res.status !== 200) {
      return { props: { error: res.statusText} }
    }

    const webhooks = await res.json();
  
    return {
      props: {
        webhooks,
      }
    }
  } catch (e) {
    console.error('Could not fetch webhooks', e.message);
    return { props: { error: e.message } }
  }
}