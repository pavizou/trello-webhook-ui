import React, { useCallback, useState } from 'react';
import nookies from 'nookies';
import Spinner from '../../components/spinner';
import Title from '../../components/title';

const WebhookForm = ({ modelId, description = '', callbackURL = '', active = true, onEditDone, id }) => {
  const [formDescription, setFormDescription] = useState(description);
  const [formActive, setFormActive] = useState(active);
  const [formCallbackURL, setFormCallbackURL] = useState(callbackURL);
  const [isSaving, setIsSaving] = useState(false);
  const [error, setError] = useState(null);

  const onFormChange = useCallback((e) => {
    if (e.target.name === 'description') setFormDescription(e.target.value);
    if (e.target.name === 'callbackURL') setFormCallbackURL(e.target.value);
    if (e.target.name === 'active') setFormActive(e.target.checked)
  }, []);

  const onClickSave = async () => {   
    setIsSaving(true);
    setError(null);
    const res = await fetch(`/api/webhooks/${id || modelId}`, {
      // If we have 'id', it means we're editing an existing hook, if not, we're creating a new one
      method: id ? 'PUT' : 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        description: formDescription,
        active: formActive,
        callbackURL: formCallbackURL,
      })
    });
    setIsSaving(false);
    if (res.ok) {
      if (typeof onEditDone === 'function') {
        const webhook = await res.json();
        onEditDone(webhook);
      }
    } else {
      const text = await res.text();
      setError(text || res.statusText);
    }
  };

  return (
    <form className="container max-w-full flex flex-col flex-wrap items-center justify-center space-y-4">
      <label className="flex-grow w-11/12 lg:w-3/12 space-y-1 min-w-max" htmlFor="description"><span className="mr-2">Description</span>
        <input disabled={isSaving} className="text-gray-800 bg-gray-200 rounded p-1.5 w-full" type="text" name="description" value={formDescription} onChange={onFormChange} />
      </label>
      <label className="flex-grow w-11/12 lg:w-3/12 space-y-1 min-w-max" htmlFor="callbackURL"><span className="mr-2">Callback URL</span>
        <input disabled={isSaving} className="text-gray-800 bg-gray-200 rounded p-1.5 w-full" type="text" name="callbackURL" value={formCallbackURL} onChange={onFormChange} />
      </label>
      <label className="flex-grow w-11/12 lg:w-3/12 space-y-1 min-w-max" htmlFor="callbackURL"><span className="mr-2">Active</span>
        <input disabled={isSaving} className="text-gray-800 rounded" type="checkbox" name="active" checked={formActive} onChange={onFormChange} />
      </label>
      <button
        type="submit"
        className="bg-blue-500 inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-white"
        onClick={onClickSave}
        disabled={isSaving}
      >
        {
          isSaving && <Spinner />
        }
        Save webhook
      </button>
      {
        error && (
          <p className="text-red-600">{error}</p>
        )
      }
    </form>
  );
}

const EditableWebhook = ({ webhook, modelId }) => {
  const [isDeleting, setIsDeleting] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [displayedWebhook, setdisplayedWebhook] = useState(webhook);

  const onClickEdit = useCallback(() => {
    setIsEditing(true);
  }, []);

  const onClickDelete = useCallback(async () => {
    setIsDeleting(true);
    const res = await fetch(`/api/webhooks/${webhook.id}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
      },
    });
    if (res.ok) {
      setIsDeleted(true);
    }
  }, [webhook.id]);

  if (isDeleted === true) return null;

  return (
    <div
      // className={`text-gray-700 p-3 rounded-lg font-medium break-all flex flex-auto flex-col bg-gray-200 w-80 h-auto max-w-full mx-3 my-4 items-center justify-center
      // ${isDeleting ? 'bg-yellow-600 opacity-70' : displayedWebhook.active ? 'bg-green-400' : 'bg-gray-500'}`}
      className="flex flex-grow text-center break-all transition-all duration-300 transform hover:z-10 lg:hover:mx-3 md:hover:scale-125 shadow-cards-light dark:shadow-cards flex-col rounded bg-gray-200 dark:bg-gray-800 dark:text-gray-200 w-96 max-w-full mx-3 sm:-ml-5 my-2 md:my-4 px-4 py-8 items-center justify-center space-y-2"
    >
      {
        isEditing ? (
          <>
            <WebhookForm modelId={modelId} {...displayedWebhook} onEditDone={(editedWebhook) => {
              setdisplayedWebhook(editedWebhook);
              setIsEditing(false);
            }} />
            <button
              type="button"
              onClick={() => setIsEditing(false)}
              className="bg-red-700 inline-flex mt-2 items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-white"
            >
              Cancel
            </button>
          </>
        ) : (
          <>
            <p
              style={{
                background: 'linear-gradient(45deg, #ff1e1e, #ffd400)',
                'WebkitBackgroundClip': 'text',
              }}
              className="font-bold text-lg text-center text-transparent"
            >
              {displayedWebhook.description}
            </p>
            <p className="font-mono">
              <em>{displayedWebhook.callbackURL}</em>
            </p>
            <p><b>{displayedWebhook.consecutiveFailures}</b> Failures</p>
            <p className="space-x-2 mt-2">
              <button type="button" className="transition-colors border border-gray-400 w-auto p-2 px-3 h-10 flex-auto text-base hover:bg-gray-500 rounded-full" onClick={onClickEdit}>✏ Edit</button>
              <button type="button" className="outline-none focus:outline-none transition-colors border border-gray-400 w-auto p-2 px-3 h-10 flex-auto text-base hover:bg-gray-500 rounded-full" onClick={onClickDelete}>
                {
                  isDeleting ? <Spinner className="inline-block m-auto -mr-0.5 mb-0.5" /> : '❌'
                } Delete
              </button>
            </p>
          </>
        )
      }
    </div>
  );
}

export default function Webhooks({ webhooks, modelId }) {
  const [displayedHooks, setDisplayedHooks] = useState(webhooks);

  return (
    <div>
      <Title>Your webhooks for object #<em>{modelId}</em>: </Title>
      <div className="flex flex-wrap container max-w-full mb-4 items-center justify-center sm:ml-4">
        {
          displayedHooks?.map((webhook) => {
            return (
                <EditableWebhook webhook={webhook} key={webhook.id} modelId={modelId} />
            );
          })
        }
        {
          (typeof displayedHooks === 'undefined' || displayedHooks?.length === 0) && (
            <p>Nothing yet</p>
          )
        }
      </div>
      <div className="flex flex-wrap container max-w-full mb-4 items-center justify-center">
        <Title>Add one</Title>
        <WebhookForm modelId={modelId} onEditDone={(newHook) => {
          setDisplayedHooks((hooks) => hooks.concat(newHook));
          }} />
      </div>
    </div>
  )
}

export async function getServerSideProps(ctx) {
  const cookies = nookies.get(ctx);
  const { modelId } = ctx.query;

  try {
    const res = await fetch(`https://api.trello.com/1/tokens/${cookies.trello_tkn}/webhooks?key=${cookies.trello_app_key || process.env.NEXT_PUBLIC_TRELLO_KEY}`);

    if (res.status !== 200) {
      return { props: { error: res.statusText} }
    }

    const webhooks = await res.json();
  
    return {
      props: {
        webhooks: webhooks.filter(w => w.idModel === ctx.query.modelId),
        modelId,
      }
    }
  } catch (e) {
    console.error('Could not fetch webhooks', e.message);
    return { props: { error: e.message } }
  }
}

export { EditableWebhook }