import nookies from 'nookies';
import Error from '../components/error';
import FlexGrid from '../components/flexGrid';
import Title from '../components/title';

export default function Boards({ boards, error }) {
  if (error) {
    return (
      <Error error={error} />
    );
  }
  return (
    <div className="container max-w-full">
      <Title>Your boards: </Title>
      <FlexGrid items={boards.boards} linkPath="lists" linkKey="id" />
    </div>
  )
}

export async function getServerSideProps(ctx) {
  const cookies = nookies.get(ctx);

  try {
    const res = await fetch(`https://api.trello.com/1/members/me/?token=${cookies.trello_tkn}&key=${cookies.trello_app_key || process.env.NEXT_PUBLIC_TRELLO_KEY}&boards=open&board_fields=name,desc,descData,closed,idOrganization,pinned,url,shortUrl,prefs,labelNames,tags`);

    if (res.status !== 200) {
      const text = await res.text();
      return { props: { error: text || res.statusText} }
    }

    const boards = await res.json();
  
    return { props: { boards } }
  } catch (e) {
    console.error('Could not fetch boards', e.message);
    return { props: { error: e.message } }
  }
}
