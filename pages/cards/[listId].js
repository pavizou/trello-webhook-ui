import nookies from 'nookies';
import FlexGrid from '../../components/flexGrid';

export default function Cards({ list }) {
  return (
    <div>
      <h1 className="text-4xl font-bold text-center p-2">Your cards in <em>{list.name}</em>: </h1>
      <FlexGrid items={list.cards} linkPath="webhooks" linkKey="id" />
    </div>
  )
}

export async function getServerSideProps(ctx) {
  const cookies = nookies.get(ctx);

  try {
    const res = await fetch(`https://api.trello.com/1/lists/${ctx.query.listId}/?token=${cookies.trello_tkn}&key=${cookies.trello_app_key || process.env.NEXT_PUBLIC_TRELLO_KEY}&cards=open`);

    if (res.status !== 200) {
      const text = await res.text();
      return { props: { error: text || res.statusText} }
    }

    const list = await res.json();
  
    return { props: { list } }
  } catch (e) {
    console.error('Could not fetch list', e.message);
    return { props: { error: e.message } }
  }
}
