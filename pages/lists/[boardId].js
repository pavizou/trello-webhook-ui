import React from 'react';
import nookies from 'nookies';

import FlexGrid from '../../components/flexGrid';
import Error from '../../components/error';
import Title from '../../components/title';

export default function List({ board, error }) {
  if (error) {
    return (
      <Error error={error} />
    );
  }
  return (
    <div>
      <Title>Your lists in <em>{board.name}</em>: </Title>
      <FlexGrid items={board.lists} linkPath="cards" linkKey="id" />
    </div>
  )
}

export async function getServerSideProps(ctx) {
  const cookies = nookies.get(ctx);

  try {
    const res = await fetch(`https://api.trello.com/1/boards/${ctx.query.boardId}/?token=${cookies.trello_tkn}&key=${cookies.trello_app_key || process.env.NEXT_PUBLIC_TRELLO_KEY}&boards=open&lists=open`);

    if (res.status !== 200) {
      return { props: { error: res.statusText} }
    }

    const board = await res.json();
  
    return { props: { board } }
  } catch (e) {
    console.error('Could not fetch board', e.message);
    return { props: { error: e.message } }
  }
}
