import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html lang="en" className="dark">
        <Head>
          <meta name="description" content="A web UI to easily manage webhooks in your Trello boards" />
          <link rel="icon" type="image/svg+xml" href="/webhooks.svg"></link>
          {
            process.env.MATOMO_DOMAIN && (
              <script type="text/javascript" dangerouslySetInnerHTML={{ __html: `
                  var _paq = window._paq = window._paq || [];
                  _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
                  _paq.push(["setCookieDomain", "*.${process.env.MATOMO_COOKIE_DOMAIN}"]);
                  _paq.push(["setDomains", ["*.${process.env.MATOMO_COOKIE_DOMAIN}"]]);
                  // _paq.push(['requireCookieConsent']);
                  _paq.push(['trackPageView']);
                  _paq.push(['enableLinkTracking']);
                  (function() {
                    var u="//${process.env.MATOMO_DOMAIN}/";
                    _paq.push(['setTrackerUrl', u+'matomo.php']);
                    _paq.push(['setSiteId', '${process.env.MATOMO_SITE_ID}']);
                    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                    g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
                  })();
                `}}>
              </script>
            )
          }
        </Head>
        <body className="dark:bg-gray-700 dark:text-gray-50 bg-gray-50 text-gray-900 sm:px-6 pt-1">
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument;
