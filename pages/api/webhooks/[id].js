import nookies from 'nookies';
import { URLSearchParams } from 'url';

export default async function handler(req, res) {
  const cookies = nookies.get({ req });
  if (req.method === 'POST') {
    const formData = new URLSearchParams();
    formData.append('description', req.body.description);
    formData.append('idModel', req.query.id);
    formData.append('callbackURL', req.body.callbackURL);
    formData.append('active', req.body.active);

    const trelloResp = await fetch(`https://api.trello.com/1/tokens/${cookies.trello_tkn}/webhooks?key=${cookies.trello_app_key || process.env.NEXT_PUBLIC_TRELLO_KEY}`, {
      method: 'POST',
      body: formData,
      redirect: 'follow'
    });

    if (trelloResp.ok) {
      const json = await trelloResp.json();
      res.json(json);
    } else {
      const text = await trelloResp.text();
      res.status(400).send(text);
    }
  } else if (req.method === 'PUT') {
    const formData = new URLSearchParams();
    formData.append('description', req.body.description);
    formData.append('callbackURL', req.body.callbackURL);
    formData.append('active', req.body.active);

    const trelloResp = await fetch(`https://api.trello.com/1/webhooks/${req.query.id}?key=${cookies.trello_app_key || process.env.NEXT_PUBLIC_TRELLO_KEY}&token=${cookies.trello_tkn}`, {
      method: 'PUT',
      body: formData,
      redirect: 'follow'
    });

    if (trelloResp.ok) {
      const json = await trelloResp.json();
      res.json(json);
    } else {
      const text = await trelloResp.text();
      res.status(400).send(text);
    }
  } else if (req.method === 'DELETE') {
    const trelloResp = await fetch(`https://api.trello.com/1/webhooks/${req.query.id}?key=${cookies.trello_app_key || process.env.NEXT_PUBLIC_TRELLO_KEY}&token=${cookies.trello_tkn}`, {
      method: 'DELETE',
      redirect: 'follow'
    });

    if (trelloResp.ok) {
      const json = await trelloResp.json();
      res.send(json);
    } else {
      const text = await trelloResp.text();
      res.status(400).send(text);
    }
  }
}
