import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import { parseCookies, setCookie } from 'nookies';
import "tailwindcss/tailwind.css";
import Token from '../components/token';
import Spinner from '../components/spinner';
// import { CookieBanner } from '@palmabit/react-cookie-law';

const AppContext = React.createContext({
  token: null,
  appKey: null,
})

function MyApp({ Component, pageProps, ...props }) {
  const [token, setToken] = useState(null);
  const [appKey, setAppKey] = useState(null);
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const splitUrl = window.location.href.split('#token=');
    if (splitUrl.length > 1) {
      setCookie(null, 'trello_tkn', splitUrl[1], {
        maxAge: 12 * 30 * 24 * 60 * 60,
        path: '/',
      })
      setToken(splitUrl[1]);
    }

    const cookies = parseCookies();

    if (cookies.trello_tkn && token !== 'reset') {
      setToken(cookies.trello_tkn);
    }
  
  }, [])

  useEffect(() => {
    if (token === 'reset' || props.hasToken === false) {
      router.push('/');
    }
  }, [token]);

  useEffect(() => {
    const handleRouteChange = () => {
      setLoading(true)
    }

    const handleRouteComplete = () => {
      setLoading(false);
    }

    router.events.on('routeChangeStart', handleRouteChange)
    router.events.on('routeChangeComplete', handleRouteComplete)

    return () => {
      router.events.off('routeChangeStart', handleRouteChange)
      router.events.off('routeChangeComplete', handleRouteComplete)
    }
  }, [])


  return (
    <AppContext.Provider value={{
      token,
      appKey,
      setToken,
      setAppKey,
    }}>
      <Head>
        <title>Manage Trello webhooks</title>
      </Head>
      {
        loading && (
          <div className="bg-gray-800 text-gray-50 fixed left-0 right-0 h-full opacity-80 flex items-center justify-center z-50">
            <Spinner /> Loading
          </div>
        )
      }
      <Token />
      <Component {...pageProps} />
      {/* <CookieBanner
        className="dark:bg-gray-50 dark:text-gray-800 bg-gray-800 text-gray-50"
        styles={{
          dialog: {
            // backgroundColor: 'red',
            position: 'fixed',
            bottom: 0,
            left: 0,
            right: 0,
            padding: '10px',
          }
        }}
        // showPreferencesOption={false}
        statisticsDefaultChecked
        message="Cookie banner message"
        showDeclineButton={true}
        declineButtonText="Decline all"
        wholeDomain={true}
        onAccept = {() => {
          console.log('ACCEPT');
        }}
        onAcceptPreferences = {() => {
          console.log('onAcceptPreferences');
        }}
        onAcceptStatistics = {() => {
          console.log('onAcceptStatistics');
        }}
        onAcceptMarketing = {() => {
          console.log('onAcceptMarketing');
        }}
    /> */}
    </AppContext.Provider>
  );
};

export default MyApp
export {
  AppContext
};

MyApp.getInitialProps = async ({ ctx }) => {
  const cookies = parseCookies(ctx);
  if (typeof cookies.trello_tkn === 'undefined') {
    return {
      hasToken: false,
    };
  }

  return {
    hasToken: true,
  };
}