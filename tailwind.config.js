module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      boxShadow: {
        cards: '#000000a6 -20px 0px 19px 0px',
        'cards-light': '#0000002e -14px 0px 8px 0px',
      },
    },
  },
  variants: {
    extend: {
      animation: ['hover'],
      // padding: ['hover'],
      // width: ['hover'],
      // height: ['hover'],
      margin: ['hover'],
      zIndex: ['hover'],
    },
  },
  plugins: [],
}
