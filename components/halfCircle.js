export default function halfCircle(props) {
  return (
    <svg width="200.39mm" height="200.39mm" version="1.1" viewBox="0 0 200.39 200.39" xmlns="http://www.w3.org/2000/svg" {...props}>
    <defs>
      <linearGradient id="linearGradient849" x1="106.94" x2="106.56" y1="199.19" y2="94.494" gradientUnits="userSpaceOnUse">
      <stop stopColor="#2291fb" offset="0"/>
      <stop stopColor="#0afff7" stopOpacity="0" offset="1"/>
      </linearGradient>
    </defs>
    <g transform="translate(-5.2622 -33.988)" fillOpacity="0">
      <path d="m200.83 157.1a98.115 97.737 0 0 1-94.557 74.811 98.115 97.737 0 0 1-95.801-73.224" stroke="url(#linearGradient849)" strokeLinecap="round" strokeLinejoin="round" strokeOpacity="1" strokeWidth="13"/>
      <path transform="matrix(.26458 0 0 .26458 5.2622 33.988)" d="m6.2971 443.93c0-3.0959 0.56307-3.3182 8.4049-3.3182 7.5818 0 8.4651 0.31478 9.0192 3.2143 0.58302 3.0505 0.15483 3.2196-8.4049 3.3182-8.5144 0.0981-9.0192-0.0818-9.0192-3.2143z" strokeOpacity="0"/>
      <path transform="matrix(.26458 0 0 .26458 5.2622 33.988)" d="m2.9638 416.8c-0.52381-0.52381-0.95238-2.131-0.95238-3.5714 0-2.2487 1.2121-2.619 8.5714-2.619 4.7143 0 8.5714 0.59028 8.5714 1.3117s0.39021 2.3286 0.86713 3.5714c0.73074 1.9043-0.46768 2.2597-7.619 2.2597-4.6674 0-8.9147-0.42857-9.4386-0.95238z" strokeOpacity="0"/>
    </g>
    </svg>
    // <svg width="200.39mm" height="200.39mm" version="1.1" viewBox="0 0 200.39 200.39" xmlns="http://www.w3.org/2000/svg" {...props}>
    //   <defs>
    //     <linearGradient id="linearGradient847" x1="106.94" x2="106.24" y1="199.19" y2="116.04" gradientUnits="userSpaceOnUse">
    //     <stop stop-color="#e86209" offset="0"/>
    //     <stop stop-color="#e86209" stop-opacity="0" offset="1"/>
    //     </linearGradient>
    //   </defs>
    //   <g transform="translate(-5.2622 -33.988)">
    //     <circle cx="105.46" cy="134.18" r="97.737" fill-opacity="0" stroke="url(#linearGradient847)" stroke-linejoin="round" stroke-opacity=".97968" strokeWidth="10"/>
    //   </g>
    // </svg>

    // <svg
    //   xmlns="http://www.w3.org/2000/svg"
    //   className="absolute bottom-0 left-0"
    //   viewBox="0 0 106 57"
    //   {...props}
    // >
    //   <defs>
    //     <linearGradient id="linear" x1="0%" y1="0%" x2="100%" y2="0%">
    //       <stop offset="0%"   stopColor="#ff4700"/>
    //       <stop offset="100%" stopColor="#ff8a00"/>
    //     </linearGradient>
    //   </defs>
    //   <path d="M102 4c0 27.1-21.9 49-49 49S4 31.1 4 4" stroke="url(#linear)"></path>
    // </svg>
  )
}
