import { useContext } from 'react';
import { AppContext } from '../pages/_app';

export default function Error({ error, showReconnect = true }) {
  const context = useContext(AppContext);
  return (
    <div>
        <p className="text-red-600">
          Oh no : {error}
        </p>
        {
          showReconnect && (
            <button className="block" type="button" onClick={() => { context.setToken('reset') }}>Reconnect</button>
          )
        }
      </div>
  )
}
