import Image from 'next/image';
import Link from 'next/link';
import HalfCircle from './halfCircle';
import LockIcon from './lockIcon';
import OpenLockIcon from './openLockIcon';
import BuildingIcon from './buildingIcon';

const getIconForPermLevel = (permLevel) => {
  switch (permLevel) {
    case 'org':
      return (
        <span className="inline-block w-full text-pink-600" aria-label="Shared in organization" title="Shared in organization" >
          <BuildingIcon />
        </span>
      );
    case 'private':
      return (
        <span className="inline-block w-full text-red-600" aria-label="Private" title="Private">
          <LockIcon />
        </span>
      );
    case 'public':
    return (
      <span className="inline-block w-full text-green-600" aria-label="Public" title="Public">
        <OpenLockIcon />
      </span>
    );
    default:
      return null;
  }
}

export default function FlexGrid({ items, linkPath, linkKey }) {
  return (
    <div className="flex flex-wrap w-full items-center justify-center sm:ml-4">
        {
          items.map((item) => {
            return (
              <div
                className="flex flex-grow text-center transition-all duration-300 transform hover:z-10 lg:hover:mx-3 md:hover:scale-125 shadow-cards-light dark:shadow-cards flex-col rounded bg-gray-200 dark:bg-gray-800 dark:text-gray-200 w-96 h-72 max-w-full mx-3 sm:-ml-5 my-2 md:my-4 py-2 px-2 items-center justify-center"
                key={[item[linkKey]]}
              >
                <Link href={`/${linkPath}/${[item[linkKey]]}`}>
                  <a className="flex-shrink">
                    <div className="m-2 h-20 w-20 relative mx-auto">
                      <div
                        className="rounded-full m-auto bg-center bg-contain"
                        aria-label="Your Trello board image"
                        style={{
                          height: '62px',
                          width: '62px',
                          backgroundImage: `url(${item.prefs?.backgroundImageScaled?.[0].url || '/trello-mark-blue.svg'})`,
                        }}
                      />
                      <HalfCircle
                        // className="hover:animate-spin"
                        style={{
                          width: '71px',
                          height: '83px',
                          position: 'absolute',
                          top: '-10px',
                          left: '4px',
                        }} />
                    </div>
                    <p
                      style={{
                        background: 'linear-gradient(45deg, #ff1e1e, #ffd400)',
                        'WebkitBackgroundClip': 'text',
                      }}
                      className="font-bold text-lg text-center text-transparent"
                    >
                      {
                        item.prefs?.permissionLevel && (
                        <span className="inline-block w-5 mr-1 -ml-1 my-auto align-middle">
                          {getIconForPermLevel(item.prefs?.permissionLevel)}
                        </span>
                        )
                      }
                      {item.name}
                    </p>
                  </a>
                </Link>
                <div className="flex flex-row flex-wrap items-center justify-center space-x-3 mt-3 border-t border-gray-400 pt-3 px-1">
                  {
                    linkPath !== 'webhooks' && (
                      <Link href={`/${linkPath}/${[item[linkKey]]}`}>
                        <a className="mb-1 transition-colors border border-gray-400 w-auto p-2 h-10 flex-auto text-base hover:bg-gray-500 rounded-full" title="Open">➡ Open</a>
                      </Link>
                    )
                  }
                  {/* {
                    item.url && (
                      <Link href={item.url}>
                        <a className="mb-1 transition-colors border border-gray-400 w-auto p-2 h-10 flex-auto text-base hover:bg-gray-500 rounded-full" target="_blank" title="See on Trello">🔗 See on Trello</a>
                      </Link>
                    )
                  } */}
                  <Link href={`/webhooks/${[item[linkKey]]}`}>
                    <a className="align-middle mb-1 transition-colors border border-gray-400 w-auto p-2 h-10 flex-auto text-base hover:bg-gray-500 rounded-full" title="Manage webhooks for this object">
                      <Image src="/webhooks.svg" alt="Webhook icon" width={24} height={24} layout="fixed" /><span className="align-top">{' '}Manage webhooks</span></a>
                  </Link>
                </div>
              </div>
            );
          })
        }
      </div>
  )
}
