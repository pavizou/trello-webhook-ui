import React from 'react'

export default function Title({ children }) {
  return (
    <h1 className="text-4xl font-bold text-center p-2 break-all my-4">{children}</h1>
  )
}
