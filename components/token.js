import { useContext } from 'react';
import Link from 'next/link';
import { AppContext } from '../pages/_app';

export default function Token() {
  const context = useContext(AppContext);
  return (
    <div className="flex flex-row flex-wrap p-2 items-center space-y-2 space-x-2">
      <Link href="/">
        <a className="text-3xl">🏠</a>
      </Link>
      {
        context.token && (
          <>
            <label htmlFor="currentToken"><span className="mr-2">Your token is</span>
              <input
                type="text"
                name="currentToken"
                onClick={(e) => e.target.select()}
                readOnly
                value={context.token}
                className="font-mono w-1/4 min-w-max max-w-full overflow-x-auto bg-gray-200 text-gray-700 p-1 rounded"
              />
            </label>
            <button className="py-1 px-4 bg-red-700 text-white rounded-md font-medium" type="button" onClick={() => { context.setToken('reset') }}>Reset token</button>
            <Link href="/webhooks/all">
              <a>
                <button className="py-1 px-4 block bg-blue-600 text-white rounded-md font-medium" type="button">All webhooks</button>
              </a>
            </Link>
          </>
        )
      }
    </div>
  )
}
